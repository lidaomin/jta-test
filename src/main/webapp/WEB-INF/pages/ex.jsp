<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Exception</title>
<link rel="stylesheet" href="/css/bootstrap.css">
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.js"></script>
</head>
<body>
	<div class="container">
		<h2 style="color: red;">发生了一些错误...</h2>
		<h3>${error }</h3>
	</div>
</body>
</html>
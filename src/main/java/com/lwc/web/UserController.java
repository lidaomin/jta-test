package com.lwc.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lwc.model.User;

@Controller
public class UserController {

	@Autowired
	UserService userService;
	
	@RequestMapping("/insert1")
	@ResponseBody
	public String insert(){
		User user = new User();
		user.setName("vvvvvv");
		userService.insert1(user);
		return "ok";
	}
}

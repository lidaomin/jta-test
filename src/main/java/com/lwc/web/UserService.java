package com.lwc.web;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lwc.mapper.local.UserMapper;
import com.lwc.mapper.remote.RemoteUserMapper;
import com.lwc.model.User;

@Service
public class UserService {

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private RemoteUserMapper remoteUserMapper;

	@Transactional
	public void insert1(User user) {
		userMapper.insertSelective(user);
		remoteUserMapper.insertSelective(user);
//		System.out.println(1/0);
	}
}

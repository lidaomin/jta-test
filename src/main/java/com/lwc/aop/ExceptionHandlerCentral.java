package com.lwc.aop;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理类
 *
 * @author lwc
 */
@ControllerAdvice
public class ExceptionHandlerCentral {

    private static final Logger logger = Logger.getLogger(ExceptionHandlerCentral.class);

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Object resolveException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        logger.error(ex.getMessage(), ex);
        Map<String, Object> map = new HashMap<>();
        map.put("ex", ex.getClass().getSimpleName());
        map.put("message", ex.getMessage());
        return map;
    }
}

package com.lwc.bean;

public enum ResultCode {

    OK(200, "成功"), DEFAULT_ERROR(500, "异常");

    private int code;
    private String desc;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private ResultCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

}

package com.lwc.bean;

import java.io.Serializable;

import lombok.Data;

@Data
public class Result implements Serializable {

	private static final long serialVersionUID = -4048889927468931833L;

	private int code;
	private String msg;
	private Object data;

	public static Result success() {
		return new Result(ResultCode.OK);
	}

	public static Result success(Object data) {
		Result result = new Result(ResultCode.OK);
		result.setData(data);
		return result;
	}

	public static Result fail(ResultCode resultCode) {
		return new Result(resultCode);
	}

	public static Result fail(String msg) {
		Result result = new Result(ResultCode.DEFAULT_ERROR);
		result.setMsg(msg);
		return result;
	}

	public Result(int code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	public Result(ResultCode resultCode) {
		this(resultCode.getCode(), resultCode.getDesc());
	}

	public Result() {
		super();
	}
}

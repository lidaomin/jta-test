package com.lwc.mapper.remote;

import com.lwc.model.User;

import tk.mybatis.mapper.common.Mapper;

public interface RemoteUserMapper extends Mapper<User> {

}

package com.lwc.mapper.local;

import com.lwc.model.User;

import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {

}
